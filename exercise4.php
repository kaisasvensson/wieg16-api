<?php
require "db.php";

$id = $_GET['customer_id'];
$address = isset($_GET['address']) ? $_GET['address'] : null;

$address_sql = "SELECT `street`, `postcode`, `city`
                FROM `customer_address`
                WHERE `customer_id` = ".$id;

$stmt = $pdo->query($address_sql);
$stmt->execute();
$addresses = $stmt->fetch();

header("Content-Type: application/json");
if ($addresses != null) {
  echo json_encode($addresses);
}
else {
  header("HTTP/1.1 404 not found");
  echo json_encode(["message" => "Customer Address is not registered"]);
}
<?php
require "db.php";

$id = $_GET['company_id'];

$sql = "SELECT * FROM customer WHERE company_id = ".$id;
$stmt = $pdo->query($sql);
$stmt->execute();
$customers =$stmt->fetchAll();

foreach ($customers as $key => $customer) {
  $sql = "SELECT * FROM customer_address WHERE customer_id = ".$customer['id'];
  $query = $pdo->query($sql);
  $address = $query->fetch();
  if ($address != null) {
    $customers[$key]['address'] = $address;
  }
}

header("Content-Type: application/json");
if ($customers != null) {
  echo json_encode($customers);
}
else {
  header("HTTP/1.1 404 not found");
  echo json_encode(["message" => "Company id do not exist"]);
}